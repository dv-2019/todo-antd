import {combineReducers} from 'redux'
import {todoReducer} from './todoReducer'
import {userReducer} from './userReducer'
import { albumReducer } from './albumReducer'
import { authReducer } from './authReducer'
export const rootReducer = combineReducers({
    todo :todoReducer,
    user:userReducer,
    album:albumReducer,
    auth:authReducer
})