import {
    Fetch_Album_Begin, Fetch_Album_Success, Fetch_Album_Error,
    Fetch_User_Begin, Fetch_User_Success, Fetch_User_Error,
    Fetch_Photos_Begin, Fetch_Photos_Success, Fetch_Photos_Error
} from '../action/albumAction'
export const initialState = {
    albums: [],
    user: {},
    photos: [],
    loading: false,
    error: ''
};
export const albumReducer = (state = initialState, action) => {
    switch (action.type) {
        case Fetch_Album_Begin:
            return {
                ...state,
                loading: true
            }
        case Fetch_Album_Success:
            return {
                ...state,
                loading: false,
                albums: action.albumdata
            }
        case Fetch_Album_Error:
            return {
                ...state,
                error: action.error
            }
        case Fetch_User_Begin:
            return {
                ...state,
                loading: true
            }
        case Fetch_User_Success:
            return {
                ...state,
                loading: false,
                user: action.userdata
            }
        case Fetch_User_Error:
            return {
                ...state,
                error: action.error
            }
        case Fetch_Photos_Begin:
            return {
                ...state,
                loading: true
            }
        case Fetch_Photos_Success:
            return {
                ...state,
                loading: false,
                photos: action.photos
            }
        case Fetch_Photos_Error:
            return {
                ...state,
                error: action.error
            }
        default:
            return state
    }
}