import { Fetch_Todo_Begin, Fetch_Todo_Success, Fetch_Todo_Error, Fetch_User_Begin, Fetch_User_Success, Fetch_User_Error, Change_Complete, Filter_Value, Search_Todo } from '../action/todoAction'
export const initialState = {
    todos: [],
    user: {},
    selectedFilter: 'All',
    text: '',
    loading: false,
    error: ''
};
export const todoReducer = (state = initialState, action) => {
    switch (action.type) {
        case Fetch_Todo_Begin:
            return {
                ...state,
                loading: true
            }
        case Fetch_Todo_Success:
            return {
                ...state,
                loading: false,
                todos: action.data
            }
        case Fetch_Todo_Error:
            return {
                ...state,
                error: action.error
            }
        case Fetch_User_Begin:
            return {
                ...state,
                loading: true
            }
        case Fetch_User_Success:
            return {
                ...state,
                loading: false,
                user: action.userdata
            }
        case Fetch_User_Error:
            return {
                ...state,
                error: action.error
            }
        case Change_Complete:
            let newList = [...state.todos];
            newList[action.index].completed = true;
            state.todos = newList;
            return {...state};

        case Filter_Value:
            state.selectedFilter = action.value;
            return {...state};
        case Search_Todo:
            state.text = action.text;
            return {...state};
        default:
            return state
    }
}
