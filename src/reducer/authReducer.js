import { Login, Logout } from "../action/authAction";
export const initialState = {
    localStorage: localStorage.getItem("isLogin")
};
export const authReducer = (state = initialState, action) => {
    switch (action.type) {
        case Login:
            localStorage.setItem("isLogin", true)
            return {
                ...state,
                localStorage: localStorage.getItem("isLogin")
            }
        case Logout:
            localStorage.setItem("isLogin", false)
            return {
                ...state,
                localStorage: localStorage.getItem("isLogin")
            }
        default:
            return state;
    }
}