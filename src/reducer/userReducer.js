import { Search_User, Fetch_Users_Begin,Fetch_Users_Success,Fetch_Users_Error } from '../action/userAction'
export const initialUsers = {
    users: [],
    text: '',
    loading: false,
    error: ''
};
export const userReducer = (state = initialUsers, action) => {
    switch (action.type) {
        case Fetch_Users_Begin:
            return {
                ...state,
                loading: true
            }
        case Fetch_Users_Success:
            return {
                ...state,
                users: action.usersdata
            }
        case Fetch_Users_Error:
            return {
                ...state,
                error: action.error
            }
        case Search_User:
            state.text = action.text;
            return { ...state }
        default:
            return state
    }
}