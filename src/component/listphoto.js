import React from 'react';
import { List, Card } from 'antd';

const { Meta } = Card;

const ListPhoto = (props) => {
    return (
        <List
            grid={{ gutter: 16, column: 4 }}
            dataSource={props.data}
            renderItem={item => (
                <List.Item>
                    <Card
                        style={{ width: 300 }}
                        cover={
                            <img
                                alt={item.title}
                                src={item.url}
                            />
                        }
                    >
                        <Meta
                            title={item.title}       
                        />
                    </Card>
                </List.Item>
            )}
        />
    )
}
export default ListPhoto;