import React from 'react';
import { List, Avatar, Skeleton } from 'antd';
const ListUser = (props) => {
    return (
        <List
            className="demo-loadmore-list"
            itemLayout="horizontal"
            bordered
            dataSource={props.data}
            renderItem={item => (
                <List.Item
                    actions={[<a key="list-loadmore-edit" href={"/users/" + item.id + "/todos/"}>Todo</a>, <a key="list-loadmore-more" href={"/users/" + item.id + "/albums"}>Albums</a>]}
                >
                    <Skeleton avatar title={false} loading={item.loading} active>
                        <List.Item.Meta
                            avatar={
                                <Avatar src="https://zos.alipayobjects.com/rmsportal/ODTLcjxAfvqbxHnVXCYX.png" />
                            }
                            title={<a href="https://ant.design">{item.name}</a>}
                            description={item.email}
                        />
                        {item.phone}&nbsp; |
                        &nbsp;{item.website}
                    </Skeleton>
                </List.Item>
            )}
        />
    );
}
export default ListUser;