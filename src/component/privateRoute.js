import React from 'react';
import { Route, Redirect } from 'react-router-dom';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { login, logout } from '../action/authAction.js'
const PrivateRoute = ({ path, component: Component, localStorage,exact }) => (
    <Route path={path} exact={exact} render={(props) => {
        // alert(props.loggedIn);
        console.log(localStorage)
        // const isLogin = localStorage.getItem("isLogin")
        if (localStorage == 'true') {
            // alert(true)
            return <Component {...props} />
        }
        else {
            // alert(false)
            return <Redirect to="/login" />
        }
    }} />
)

const mapStateToProps = state => {
    return {
        localStorage: state.auth.localStorage
    }
}
const mapDispatchToProps = dispatch => {
    return bindActionCreators({ login, logout }, dispatch)
}
export default connect(mapStateToProps, mapDispatchToProps)(PrivateRoute);