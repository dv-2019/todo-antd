import React from 'react';
import { List} from 'antd';
const ListAlbum = (props) => {
    return (
        <List
            size="large"
            header={<div>Albums</div>}
            footer={<div>{"Item:" + props.length}</div>}
            bordered
            dataSource={props.data}
            renderItem={item =>
                <List.Item ><a key="list-loadmore-edit" href={"/users/" + props.userId + "/albums/" + item.id}>{item.title}</a></List.Item>
            }
        />
    )
}
export default ListAlbum;