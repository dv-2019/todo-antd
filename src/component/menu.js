import React from 'react';
import { Layout, Menu } from 'antd';
const { Header } = Layout;
const MenuBar = () => {
    return (
        <Header>
            <Menu
                theme="dark"
                mode="horizontal"
                style={{ lineHeight: '64px' }}
            >
                <Menu.Item key="1"><a href="/users" >Home</a></Menu.Item>
            </Menu>
        </Header>
    )
}
export default MenuBar;