import React from 'react';
import { List, Typography } from 'antd'
import { Button } from 'antd';;
const ListTodo = (props) => {
    return (
        <List
            bordered
            dataSource={props.data}
            renderItem={(item) => (
                <List.Item>
                    <div>
                        {item.completed ?
                            <Typography.Text delete>DONE</Typography.Text>
                            :
                            <Typography.Text mark>DOING</Typography.Text>
                        }
                        &nbsp;
                        {item.title}
                    </div>
                    {item.completed ?
                        <div></div>
                        :
                        <Button onClick={() => { props.change(item.id - 1) }} type="primary">Done</Button>
                    }
                </List.Item>
            )}
        />
    )
}
export default ListTodo;