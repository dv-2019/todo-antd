import React, { useEffect } from 'react';
import { Layout, PageHeader,Button } from 'antd';
import ListAlbum from '../component/listAlbum';
import { connect } from "react-redux"
import { fetchAlbum } from '../action/albumAction'
import { bindActionCreators } from 'redux';
const { Content } = Layout;
const AlbumPage = (props) => {
    
    const { fetchAlbum, albums } = props;

    useEffect(() => {
        fetchAlbum(props.match.params.user_id);
    }, [])

    return (
        <>
            <PageHeader
                style={{
                    border: '1px solid rgb(235, 237, 240)',
                    backgroundColor: '#e8e8e8'
                }}
                onBack={() => (window.location.pathname = ("/users"))}
                subTitle="Back to users list"
                extra={[
                    <Button type="primary">
                        <a href="/processLogout" >Logout</a>
                    </Button>
                ]}
            />
            <div className="layout">
                <Content style={{ padding: '0 50px' }}>
                    <br />
                    <ListAlbum data={albums} length={albums.length} userId={props.match.params.user_id} />
                </Content>
            </div>
        </>

    )
}
const mapStateToProps = state => {
    return {
        albums: state.album.albums
    }
}
const mapDispatchToProps = dispatch => {
    return bindActionCreators({ fetchAlbum }, dispatch)
}
export default connect(mapStateToProps, mapDispatchToProps)(AlbumPage);