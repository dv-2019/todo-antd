import React, { useEffect } from 'react';
import { Descriptions, Select, PageHeader, Input,Button } from 'antd';
import ListTodo from '../component/listTodo';
import { fetchTodo, fetchUser, changeCompleted, filterValue, searchTodo } from '../action/todoAction'
import { connect } from "react-redux"
import { bindActionCreators } from 'redux';

const { Search } = Input;
const { Option } = Select;
const TodoPage = (props) => {

    const { fetchTodo, fetchUser, changeCompleted, filterValue, searchTodo } = props;
    const { todo, user, selectedFilter, text } = props;

    useEffect(() => {
        fetchTodo()
        fetchUser(props.match.params.user_id)
    }, [])

    return (
        <>
            <PageHeader
                style={{
                    border: '1px solid rgb(235, 237, 240)',
                    backgroundColor: '#e8e8e8'
                }}
                onBack={() => (window.location.pathname = ("/users"))}
                subTitle="Back to users list"
                extra={[
                    <Button type="primary">
                        <a href="/processLogout" >Logout</a>
                    </Button>
                ]}
            />
            <div style={{ padding: '0 50px' }}>
                <br />
                <Descriptions bordered title={"Name: " + user.name} >
                    <Descriptions.Item label="UserName">{user.username}</Descriptions.Item>
                    <Descriptions.Item label="Email">{user.email}</Descriptions.Item>
                    <Descriptions.Item label="Phone">{user.phone}</Descriptions.Item>
                    <Descriptions.Item label="Website">{user.website}</Descriptions.Item>
                    <Descriptions.Item label="Address">
                        {user.address !== undefined ?
                            <div>
                                {user.address.street}
                                ,
                                {user.address.suite}
                                ,
                                {user.address.city}
                                ,
                                {user.address.zipcode}
                            </div>
                            :
                            <div></div>
                        }
                    </Descriptions.Item>
                    <Descriptions.Item label="Company">
                        {user.company !== undefined ?
                            <div>
                                {user.company.name}
                            </div>
                            :
                            <div></div>
                        }
                    </Descriptions.Item>
                </Descriptions>
                <br />
                <Search
                    placeholder="input search text"
                    onChange={e => searchTodo(e.target.value)}
                    style={{ width: 200, margin: '0 45%' }}
                />
                <Select defaultValue="All" style={{ width: 120 }} onChange={(value) => filterValue(value)}>
                    <Option value="All">All</Option>
                    <Option value="false">DOING</Option>
                    <Option value="true">DONE</Option>
                </Select>
                <br />
                <br />
                <ListTodo
                    data={
                        selectedFilter === 'All' ?
                            todo.filter(t => t.userId === parseInt(props.match.params.user_id)).filter(t => t.title.toLowerCase().match(text.toLowerCase()))
                            :
                            todo.filter(t => t.userId === parseInt(props.match.params.user_id)).filter(t => t.completed.toString() === selectedFilter).filter(t => t.title.toLowerCase().match(text.toLowerCase()))
                    }
                    change={(index) => changeCompleted(index)}
                />

            </div>
        </>
    )
}
const mapStateToProps = state => {
    return {
        todo: state.todo.todos,
        user: state.todo.user,
        selectedFilter: state.todo.selectedFilter,
        text: state.todo.text
    }
}
const mapDispatchToProps = dispatch => {
    return bindActionCreators({ fetchTodo, fetchUser, changeCompleted, filterValue, searchTodo }, dispatch)
}
export default connect(mapStateToProps, mapDispatchToProps)(TodoPage);