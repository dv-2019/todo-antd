import React, { useEffect } from 'react';
import ListUser from '../component/listUser';
import { fetchUsers, searchUser } from '../action/userAction'
import { Input, PageHeader,Button } from 'antd';
import { connect } from "react-redux"
import { bindActionCreators } from 'redux';
const { Search } = Input;
const Userpage = (props) => {

    const { fetchUsers, searchUser, users, text } = props;

    useEffect(() => {
        fetchUsers();
    }, [])

    return (
        <>
            <PageHeader
                style={{
                    border: '1px solid rgb(235, 237, 240)',backgroundColor: '#e8e8e8'
                }}
                title="Users Page"
                extra={[
                    <Button type="primary">
                        <a href="/processLogout" >Logout</a>
                    </Button>
                ]}
            />
            <div style={{ padding: '0 50px' }}>
                <br />
                <Search
                    placeholder="input search text"
                    onChange={e => searchUser(e.target.value)}
                    style={{ width: 200, margin: '0 45%' }}
                />
                <br />
                <br />
                <ListUser data={text === '' ? users : users.filter(user => user.name.toLowerCase().match(text.toLowerCase()))} />
            </div>
        </>

    )
}

const mapStateToProps = state => {
    return {
        users: state.user.users,
        text: state.user.text
    }
}

const mapDispatchToProps = dispatch => {
    return bindActionCreators({ fetchUsers, searchUser }, dispatch)
}

export default connect(mapStateToProps, mapDispatchToProps)(Userpage);