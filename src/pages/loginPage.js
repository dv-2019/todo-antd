import React from 'react';
import { Row, Col, Form, Icon, Input, Button, Checkbox } from 'antd';

const LoginPage = () => {

    return (
        <>
            <div style={{ padding: '0 50px', textAlign: 'center', margin: '0 auto' }}>
                <Row>
                    <Col span={12} offset={6}>

                        <p>Login</p>
                        <Input
                            prefix={<Icon type="user" style={{ color: 'rgba(0,0,0,.25)' }} />}
                            placeholder="Username"
                        />
                        <br />
                        <br />
                        <Input
                            prefix={<Icon type="lock" style={{ color: 'rgba(0,0,0,.25)' }} />}
                            type="password"
                            placeholder="Password"
                        />
                        <br />
                        <br />
                        <Button type="primary" >
                            <a href="/processLogin" >Log In</a>
                        </Button>

                    </Col>
                </Row>

            </div>

        </>
    )
}
export default LoginPage;