import React, { useEffect } from 'react';
import { Descriptions,PageHeader,Button } from 'antd';
import ListPhoto from '../component/listPhoto';
import { connect } from 'react-redux'
import { fetchUser, fetchAlbum, fetchPhotos } from '../action/albumAction'
import { bindActionCreators } from 'redux';
const AlbumListPage = (props) => {

    const { fetchUser, fetchAlbum, fetchPhotos } = props;
    const { user, albums, photos } = props;
    
    useEffect(() => {
        fetchUser(props.match.params.user_id);
        fetchAlbum(props.match.params.album_id);
        fetchPhotos(props.match.params.album_id);
    }, [])

    console.log(albums)
    return (
        <>
            <PageHeader
                style={{
                    border: '1px solid rgb(235, 237, 240)',
                    backgroundColor: '#e8e8e8'
                }}
                onBack={() => (window.location.pathname = ("/users/" + user.id + "/albums"))}
                subTitle="Back to album list"
                extra={[
                    <Button type="primary">
                        <a href="/processLogout" >Logout</a>
                    </Button>
                ]}
            />
            <div style={{ padding: '0 50px' }}>
                <br />
                <h3 style={{ textAlign: 'center' }}>
                    {albums[props.match.params.album_id - 1] !== undefined ?
                        <div>
                            Gallery Name: {albums[props.match.params.album_id - 1].title}
                        </div>
                        :
                        <div></div>
                    }
                </h3>
                <br />
                <Descriptions bordered title={"Name: " + user.name} >
                    <Descriptions.Item label="UserName">{user.username}</Descriptions.Item>
                    <Descriptions.Item label="Email">{user.email}</Descriptions.Item>
                    <Descriptions.Item label="Phone">{user.phone}</Descriptions.Item>
                    <Descriptions.Item label="Website">{user.website}</Descriptions.Item>
                    <Descriptions.Item label="Address">
                        {user.address !== undefined ?
                            <div>
                                {user.address.street}
                                ,
                                {user.address.suite}
                                ,
                                {user.address.city}
                                ,
                                {user.address.zipcode}
                            </div>
                            :
                            <div></div>
                        }
                    </Descriptions.Item>
                    <Descriptions.Item label="Company">
                        {user.company !== undefined ?
                            <div>
                                {user.company.name}
                            </div>
                            :
                            <div></div>
                        }
                    </Descriptions.Item>
                </Descriptions>
                <br />
                <ListPhoto data={photos} />
            </div>
        </>
    )
}
const mapStateToProps = state => {
    return {
        user: state.album.user,
        albums: state.album.albums,
        photos: state.album.photos
    }
}
const mapDispatchToProps = dispatch => {
    return bindActionCreators({ fetchUser, fetchAlbum, fetchPhotos }, dispatch)
}
export default connect(mapStateToProps, mapDispatchToProps)(AlbumListPage);