
export const Fetch_Users = 'FETCH_USERS';
export const Search_User = 'SEARCH_USER';
export const Fetch_Users_Begin = 'FETCH_USERS_BEGIN';
export const Fetch_Users_Success = 'FETCH_USERS_SUCCESS';
export const Fetch_Users_Error = 'FETCH_USERS_ERROR';
export const fetchUsers = () => {
    return dispatch => {
        dispatch(fetchUsersBegin())
        return fetch('https://jsonplaceholder.typicode.com/users/')
            .then(response => response.json())
            .then(data => {
                dispatch(fetchUsersSuccess(data))
            })
            .catch(error => {
                dispatch(fetchUsersError(error))
            })
    }
}

export const searchUser = (text) => {
    return {
        type: Search_User,
        text
    }
}
export const fetchUsersBegin = () => {
    return {
        type: Fetch_Users_Begin
    }
}
export const fetchUsersSuccess = (usersdata) => {
    return {
        type: Fetch_Users_Success,
        usersdata
    }
}
export const fetchUsersError = (error) => {
    return {
        type: Fetch_Users_Error,
        error
    }
}