export const Fetch_Todo = 'FETCH_TODO';
export const Fetch_User = 'FETCH_USER';
export const Change_Complete = 'CHANGE_COMPLETE';
export const Filter_Value = 'FILTER_VALUE';
export const Search_Todo = 'SEARCH_TODO';
export const Fetch_Todo_Begin = 'FETCH_TODO_BEGIN';
export const Fetch_Todo_Success = 'FETCH_TODO_SUCCESS';
export const Fetch_Todo_Error = 'FETCH_TODO_ERROR';
export const Fetch_User_Begin = 'FETCH_USER_BEGIN';
export const Fetch_User_Success = 'FETCH_USER_SUCCESS';
export const Fetch_User_Error = 'FETCH_USER_ERROR';

export const fetchTodo = () => {
    return dispatch => {
        dispatch(fetchTodoBegin())
        return fetch('https://jsonplaceholder.typicode.com/todos')
            .then(res => res.json())
            .then(data => {
                dispatch(fetchTodoSuccess(data))
            })
            .catch(err => {
                dispatch(fetchTodoError(err))
            })
    }
}

export const fetchUser = (userId) => {
    return dispatch => {
        dispatch(fetchUserBegin())
        return fetch('https://jsonplaceholder.typicode.com/users/' + userId)
            .then(response => response.json())
            .then(data => {
                dispatch(fetchUserSuccess(data))
            })
            .catch(error => {
                dispatch(fetchUserError(error))
            })
    }
}
export const changeCompleted = (index) => {
    return {
        type: Change_Complete,
        index
    }
}
export const filterValue = (value) => {
    return {
        type: Filter_Value,
        value
    }
}
export const searchTodo = (text) => {
    return {
        type: Search_Todo,
        text
    }
}

export const fetchTodoBegin = () => {
    return {
        type: Fetch_Todo_Begin
    }
}
export const fetchTodoSuccess = (data) => {
    return {
        type: Fetch_Todo_Success,
        data
    }
}
export const fetchTodoError = (error) => {
    return {
        type: Fetch_Todo_Error,
        error
    }
}

export const fetchUserBegin = () => {
    return {
        type: Fetch_User_Begin
    }
}
export const fetchUserSuccess = (userdata) => {
    return {
        type: Fetch_User_Success,
        userdata
    }
}
export const fetchUserError = (error) => {
    return {
        type: Fetch_User_Error,
        error
    }
}