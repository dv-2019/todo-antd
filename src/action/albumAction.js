export const Fetch_Album = 'FETCH_ALBUM_DATA';
export const Fetch_User = 'FETCH_USER';
export const Fetch_AlbumList = 'FETCH_ALBUM_DATA';
export const Fetch_Photos = 'FETCH_PHOTOS';

export const Fetch_Album_Begin = 'FETCH_ALBUM_BEGIN';
export const Fetch_Album_Success = 'FETCH_ALBUM_SUCCESS';
export const Fetch_Album_Error = 'FETCH_ALBUM_ERROR';

export const Fetch_User_Begin = 'FETCH_USER_BEGIN';
export const Fetch_User_Success = 'FETCH_USER_SUCCESS';
export const Fetch_User_Error = 'FETCH_USER_ERROR';

export const Fetch_AlbumList_Begin = 'FETCH_ALBUM_LIST_BEGIN';
export const Fetch_AlbumList_Success = 'FETCH_ALBUM_LIST_SUCCESS';
export const Fetch_AlbumList_Error = 'FETCH_ALBUM_LIST_ERROR'

export const Fetch_Photos_Begin = 'FETCH_PHOTOS_BEGIN';
export const Fetch_Photos_Success = 'FETCH_PHOTOS_SUCCESS';
export const Fetch_Photos_Error = 'FETCH_PHOTOS_ERROR';

export const fetchAlbum = (userId) => {
    return dispatch => {
        dispatch(fetchAlbumBegin())
        return fetch('https://jsonplaceholder.typicode.com/albums?userId=' + userId)
            .then(response => response.json())
            .then(data => {
                dispatch(fetchAlbumSuccess(data))
            })
            .catch(error => {
                dispatch(fetchAlbumError(error))
            });
    }
}
export const fetchAlbumBegin = () => {
    return {
        type: Fetch_Album_Begin
    }
}
export const fetchAlbumSuccess = (albumdata) => {
    return {
        type: Fetch_Album_Success,
        albumdata
    }
}
export const fetchAlbumError = (error) => {
    return {
        type: Fetch_Album_Error,
        error
    }
}
export const fetchUser = (userId) => {
    return dispatch => {
        dispatch(fetchUserBegin())
        return fetch('https://jsonplaceholder.typicode.com/users/' + userId)
            .then(response => response.json())
            .then(data => {
                dispatch(fetchUserSuccess(data))
            })
            .catch(error => {
                dispatch(fetchUserError(error))
            })
    }
}
export const fetchUserBegin = () => {
    return {
        type: Fetch_User_Begin
    }
}
export const fetchUserSuccess = (userdata) => {
    return {
        type: Fetch_User_Success,
        userdata
    }
}
export const fetchUserError = (error) => {
    return {
        type: Fetch_User_Error,
        error
    }
}

export const fetchPhotos = (albumId) => {
    return dispatch => {
        dispatch(fetchPhotosBegin())
        fetch('https://jsonplaceholder.typicode.com/photos?albumId=' + albumId)
            .then(response => response.json())
            .then(data => {
                dispatch(fetchPhotosSuccess(data))
            })
            .catch(error => { 
                dispatch(fetchPhotosError(error))
            })
    }
}
export const fetchPhotosBegin = () => {
    return {
        type: Fetch_Photos_Begin
    }
}
export const fetchPhotosSuccess = (photos) => {
    return {
        type: Fetch_Photos_Success,
        photos
    }
}
export const fetchPhotosError = (error) => {
    return {
        type: Fetch_Photos_Error,
        error
    }
}