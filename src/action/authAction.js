export const Login = 'LOGIN';
export const Logout = 'LOGOUT';
export const login = () => {
    return {
        type: Login
    }
}
export const logout = () => {
    return {
        type: Logout
    }
}