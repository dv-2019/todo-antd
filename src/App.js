import React from 'react';
import './App.css';
import Userpage from './pages/userPage';
import TodoPage from './pages/todoPage'
import { BrowserRouter, Route, Redirect } from 'react-router-dom';
import AlbumPage from './pages/albumPage';
import AlbumListPage from './pages/albumListpage'
import PrivateRoute from './component/privateRoute';
import LoginPage from './pages/loginPage';
import { login, logout } from './action/authAction.js'
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

const App = (props) => {
  const { login, logout } = props;
  return (
    <BrowserRouter>
      <Route path="/processLogin" render={() => {
        login()
        return <Redirect to="/users" />
      }} />
      <Route path="/processLogout" render={() => {
        logout()
        return <Redirect to="/login" />
      }} />
      <Route path="/" component={LoginPage} exact={true} />
      <Route path="/login" component={LoginPage} />
      <PrivateRoute path="/users" component={Userpage} exact={true} />
      <PrivateRoute path="/users/:user_id/todos" component={TodoPage} />
      <PrivateRoute path="/users/:user_id/albums" component={AlbumPage} exact={true} />
      <PrivateRoute path="/users/:user_id/albums/:album_id" component={AlbumListPage} />
    </BrowserRouter>
  );
}
const mapStateToProps = state => {
  
}
const mapDispatchToProps = dispatch => {
  return bindActionCreators({ login, logout }, dispatch)
}
export default connect(mapStateToProps, mapDispatchToProps)(App);
